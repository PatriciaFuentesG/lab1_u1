/*
g++ ejercicio1.cpp -o ejercicio1
./ejercicio1 N

Escriba un programa que llene un arreglo unidimensional de números enteros y luego obtenga como
resultado la suma del cuadrado de los números ingresados. (Considere un arreglo unidimensional de
tipo entero de N elementos).
*/

#include <iostream>
#include <math.h>
using namespace std;

void imprime_vector(int *vec, int n)
{   cout << endl;
    cout << "Elementos del vector: "<< endl;
    for (int i = 0; i < n; i++)
    {
        cout << vec[i] << " ";
    }
    cout << endl;
}

void inicializa_vector(int *vec, int n)
{   
    for (int i = 0; i < n; i++)
    {
        vec[i] = i;
    }
}
//Se encarga de realizar la operacion de suma del cuadrado de un numero 
void suma_de_cuadrados(int *vec, int n){ 
    int num;
    int sum;

    for (int i = 0; i < n; i++)
    {   
        num = vec[i];
        sum = pow(num,2) + pow(num,2);
        vec[i] = sum ;      
        cout << vec[i] << " ";
    }
}

int main(int argc, char **argv)
{
    int n = atoi(argv[1]);

    int vector1[n];

    inicializa_vector(vector1, n);

    imprime_vector(vector1, n);
    cout << "Resultado la suma del cuadrado de los elementos del vector: "<< endl;
    suma_de_cuadrados(vector1,n);
    cout << endl;

    
    return 0;
}
