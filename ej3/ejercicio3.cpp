/*
g++ ejercicio3.cpp -o ejercicio3
./ejemplos n
*/

#include <iostream>
#include <vector>
#include <string>
#include <cstring> 
#include <list>
#include <algorithm>
#include "Cliente.h"
using namespace std;


void llenar_datos(vector <Cliente> array, int n){

    string True = "true";
    string False = "false";
    string nombre ;
    string telefono ;
    int saldo;
    bool moroso; 
    string morosos; 

   for (int i = 0; i < n; ++i) {
        
        cout << "Ingrese el nombre del cliente:"<<endl;
        cin >> nombre;
        cout << "Ingrese el numero telefonico del cliente:"<<endl;
        cin >> telefono;
            
        cout <<"Ingrese el saldo del cliente:"<< endl;
        cin >>saldo;
            
        cout<< "Ingrese si o no moroso(en caso de ser moroso poner 'true', en el caso contrario poner 'false':"<<endl;
        cin >> morosos;
       
        if (0 == strcmp(True.c_str(),morosos.c_str()) ){ 
            moroso = true;
        }else if(0 == strcmp(False.c_str(),morosos.c_str())){
            moroso =false;
        }else{
            //Esto pasa si el usuario no escribio 'true' o 'false', el programa asumira que es false
            cout<<"Lo escrito de manera inadecuada"<<endl;
        }
         cout << endl;
        array.push_back(Cliente(nombre,telefono,saldo,moroso));
        array[i].imprimir();
     
    }

      

}




int main (int argc, char **argv){
    int n = atoi(argv[1]);

    if( n < 2){
        cout<< "N debe ser mayor o igual a 2"<<endl;
        return -1;
    }
    
   
   vector <Cliente> array;
   llenar_datos(array,n);
  

 return 0;
}