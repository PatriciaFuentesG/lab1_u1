#ifndef CLIENTE_H
#define CLIENTE_H

#include <iostream>
using namespace std;


class Cliente {


    private:
       string nombre = "\0";
       string telefono = "\0";
       int saldo = 0;
       bool moroso = false;

    public:
        Cliente(string nombre, string telefono, int saldo,bool moroso);
        string get_nombre();
        string get_telefono();
        int get_saldo();
        bool get_moroso();
        
        void set_nombre(string nombre);
        void set_telefono(string nombre);
        void set_saldo(int saldo);
        void set_moroso(bool moroso);

        void imprimir();
        
};
#endif