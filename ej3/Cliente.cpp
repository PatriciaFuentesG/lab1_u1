#include <iostream>
using namespace std;
#include "Cliente.h"


Cliente::Cliente(string nombre, string telefono, int saldo,bool moroso) {
   this->nombre = nombre;
    this->telefono = telefono;
    this->saldo = saldo;
    this->moroso = moroso;
    
}

string Cliente::get_nombre(){
    return this->nombre;
}
string Cliente::get_telefono(){
    return this->telefono;
}
int Cliente::get_saldo(){
    return this->saldo;
}
bool Cliente::get_moroso() {
    return this->moroso;
}

void Cliente::set_moroso(bool moroso) {
    this->moroso = moroso;
}
//Se encarga de imprimir todo la informacion sobre el Cliente
void Cliente::imprimir(){
    cout << "Nombre: " << get_nombre() << endl;
    cout << "Telefono: " << get_telefono() << endl;
    cout << "Saldo: " << get_saldo() << endl;
    if (get_moroso() != false ){
        cout << "Este cliente es moroso"<< endl;
    }else if(get_moroso() == false){
        cout << "Este cliente no es moroso" << endl;
    } 
    cout << endl;    
}        
