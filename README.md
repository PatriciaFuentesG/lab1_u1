# Lab1_U1
Para poder usar esta aplicación usted debe tener IDE con leguaje c++ como Geany o Visual Studio Code, instalado en su computador.
Primero debe descargar todos los elementos de la carpeta. Abra la IDE y tendra tres carpetas cada una guarda un programa distinto.
Si abres la carpeta ej1 habra un programa que llene un arreglo unidimensional de números enteros y luego obtenga como resultado la suma del cuadrado de los números ingresados.Para ejecutarla escriba "make" y despues escriba:

        ./ejercicio1 N->Representa el numero de enteros con los que se trabajara.

Si abres la carpeta ej2 habra un programa que lee N frases en un arreglo de caracteres y que determina el número de minúsculas y mayúsculas que hay en cada una de ellas.Para ejecutarla escriba "make" y despues escriba:

        ./ejercicio2 N->Representa el numero de frases con las que se trabajara.

Si abres la carpeta ej3 habra un programa que registra informacion sobre N clientes.Para ejecutarla escriba "make" y despues escriba:

        ./ejercicio3 N->Representa el numero de clientes sobre que se recolectaran los datos.

Despues se le pediran los siguietes datos por cada Cliente:

                    Nombre ->Solo debe escribir el nombre del cliente
                    Telefono->Solo debe escribir el telefono del cliente 
                    Saldo-Solo debe escribir el saldo del cliente
                    Moroso-> Ecribir si es o no moroso , en caso que si poner true, en caso contrario poner false 



